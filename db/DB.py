import psycopg2
import config


class DB(object):
    conn = None

    """
    constructor to open database connections with values from config.py
    """
    def __init__(self):
        try:
            self.conn = psycopg2.connect("dbname='" + config.DATABASE_CONFIG['db_name'] + "' user='" +
                                    config.DATABASE_CONFIG['user'] + "' host='" + config.DATABASE_CONFIG['host'] +
                                    "' password='" + config.DATABASE_CONFIG['password'] + "'")
        except:
            print ("Could not connect to %s " % config.DATABASE_CONFIG['db_name'])

    """
    function to insert data into database
    :parameter
    - s_sql: INSERT command into table
    """
    def insert(self, s_sql=""):
        if (s_sql.upper()).startswith("INSERT"):
            cur = self.conn.cursor()
            try:
                cur.execute(s_sql)
                self.conn.commit()
            except:
                print("Could not insert your data, please check your query: " + s_sql)
            cur.close()
        else:
            print ("Only inserts allowed, please check your query: " + s_sql)

    """
    function to select data from database
    :parameter
    s_sql: SELECT command to fetch data from table
    
    :returns
    array of array of values
    """
    def select(self, s_sql=""):
        if (s_sql.upper()).startswith("SELECT"):
            cur = self.conn.cursor()
            try:
                cur.execute(s_sql)
                rows = cur.fetchall()
            except:
                print ("Could not select your data, please check your query: " + s_sql)
            cur.close()
        else:
            print ("Only selects allowed, please check your query: " + s_sql)
