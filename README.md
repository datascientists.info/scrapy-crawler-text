# Scrapy Crawler for Textgeneration

## Getting started

### Prerequistes
- install python3
- install pip3
- install requirements by running "pip3 install -r requirements.txt"
- install postgres
- adapt config.py values to your own
- run ddl.sql script in postgres to create tables

### Running the crawler
You can run the crawler by typing:  scrapy crawl "crawler_name"
Right now there are the follwing crawlers:
- about

### Licensing
MIT License
