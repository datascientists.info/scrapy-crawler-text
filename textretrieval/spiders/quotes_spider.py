import scrapy
from bs4 import BeautifulSoup
from db.DB import DB

class QuotesSpider(scrapy.Spider):
    name = "about"

    def start_requests(self):
        urls = [
            'http://datascientists.info/index.php/about/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split("/")[2]
        filename = 'about-%s.html' % page
        #print response.body
        soup = BeautifulSoup(response.text, 'lxml')
        print (soup.article.get_text())
        #with open(filename, 'wb') as f:
        #    f.write(text)
        db = DB()

        if db.conn != None:
            db.insert("INSERT INTO about_text (website, about_text, timestamp_added) " +
                      "values ('" + page + "', '" + soup.article.get_text() + "', extract(epoch from now()))")
            print ("INSERT INTO about_text (website, about_text, timestamp_added) " +
                      "values ('" + page + "', '" + soup.article.get_text() + "', extract(epoch from now()))")
        self.log('Saved database %s' % page)