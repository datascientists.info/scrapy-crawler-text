-- about page texts
create table about_text (
    id serial,
    website text,
    about_text text,
    timestamp_added bigint
);